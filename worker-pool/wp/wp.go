package wp

import (
	"context"
	"errors"
	"runtime"
	"sync/atomic"
)

// Batch represents a logical group of jobs. On failure of any job, or user cancellation, cancel further execution of
// other jobs
type Batch struct {
	ctx        context.Context
	cancelFunc context.CancelFunc
	jobs       []WorkerFunc
	results    []interface{}
}

// NewBatch creates a Batch instance
func NewBatch(ctx context.Context, cancelFunc context.CancelFunc, jobs []WorkerFunc) *Batch {
	return &Batch{
		ctx:        ctx,
		cancelFunc: cancelFunc,
		jobs:       jobs,
		results:    make([]interface{}, 0, len(jobs)),
	}
}

func (b *Batch) handleErrAndCtxCancel(err error) error {
	if err != nil {
		b.cancelFunc()
		return err
	}
	if err = b.ctx.Err(); err != nil {
		return err
	}

	return nil
}

// Run the batch in a worker Pool, blocks until all jobs finish or any job errors out
func (b *Batch) Run(wp *Pool) ([]interface{}, error) {
	trackers := make([]*Job, len(b.jobs))
	var err error

	for i, job := range b.jobs {
		trackers[i], err = wp.Queue(b.ctx, job)

		if err = b.handleErrAndCtxCancel(err); err != nil {
			return b.results, err
		}
	}

	for _, tracker := range trackers {
		var result interface{}
		result, err = tracker.Result()
		b.results = append(b.results, result)

		if err = b.handleErrAndCtxCancel(err); err != nil {
			return b.results, err
		}
	}

	return b.results, nil
}

// WorkerFunc represents a function that you want to run in the worker Pool
type WorkerFunc func(ctx context.Context) (interface{}, error)

// Job represents a wrapper around WorkerFunc used by worker Pool
type Job struct {
	id int64

	ctx      context.Context
	work     WorkerFunc
	doneChan chan struct{}

	err    error
	result interface{}
}

// Result awaits for the job to be finish running and get its result
func (j *Job) Result() (interface{}, error) {
	<-j.doneChan
	return j.result, j.err
}

// ID returns id of a job
func (j *Job) ID() int64 {
	return j.id
}

func (j *Job) run() {
	defer close(j.doneChan)
	if err := j.ctx.Err(); err != nil {
		j.err = err
		return
	}
	j.result, j.err = j.work(j.ctx)
	j.doneChan <- struct{}{}
}

func newJob(ctx context.Context, id int64, workFunc WorkerFunc) *Job {
	return &Job{
		id:       id,
		ctx:      ctx,
		work:     workFunc,
		doneChan: make(chan struct{}, 1),
	}
}

// Pool is a worker pool, this is the main entity of this package
type Pool struct {
	workers      int
	jobQueueSize int
	started      bool
	currID       int64

	logger   Logger
	workFunc func(int)

	stopChan chan struct{}
	jobQueue chan *Job
}

// PoolOptions represents configuration for a worker Pool
type PoolOptions struct {
	Workers  int
	JobQueue int
	Logger   Logger
}

// NewPool creates a new worker Pool
func NewPool(opts PoolOptions) *Pool {
	var logger Logger
	logger = NoopLogger{}
	if opts.Logger != nil {
		logger = opts.Logger
	}

	workers := opts.Workers
	if workers <= 0 {
		workers = runtime.GOMAXPROCS(0)
	}

	jobQueue := opts.JobQueue
	if jobQueue <= 0 {
		jobQueue = workers
	}

	return &Pool{
		workers:      workers,
		jobQueueSize: jobQueue,
		logger:       logger,
	}
}

// Start workers that will process jobs
func (p *Pool) Start() bool {
	if p.started {
		return false
	}
	p.jobQueue = make(chan *Job, p.jobQueueSize)
	p.stopChan = make(chan struct{})

	for i := 0; i < p.workers; i++ {
		go func(i int) {
			p.work(i)
		}(i)
	}
	p.started = true
	return true
}

// Stop all workers, and blocks until all workers are stopped
func (p *Pool) Stop() bool {
	if !p.started {
		return false
	}
	for i := 0; i < p.workers; i++ {
		p.stopChan <- struct{}{}
	}
	close(p.jobQueue)
	close(p.stopChan)
	p.started = false
	return true
}

// Queue will add a job to the Pool. It will block until it can add to the jobQueue channel
func (p *Pool) Queue(ctx context.Context, workFunc WorkerFunc) (*Job, error) {
	if !p.started {
		return nil, errors.New("worker pool is not started")
	}

	job := newJob(ctx, atomic.AddInt64(&p.currID, 1), workFunc)
	p.jobQueue <- job

	return job, nil
}

func (p *Pool) work(i int) {
	p.logger.Info("starting worker", "workerID", i)
	for {
		select {
		case <-p.stopChan:
			p.logger.Info("stopping worker", "workerID", i)
			return
		case job := <-p.jobQueue:
			p.logger.Debug("running new job", "workerID", i, "job.ID", job.ID())
			job.run()
		}
	}
}
