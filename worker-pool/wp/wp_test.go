package wp

import (
	"context"
	"errors"
	"fmt"
	"reflect"
	"testing"
	"time"

	"go.uber.org/goleak"
)

func TestMain(m *testing.M) {
	goleak.VerifyTestMain(m)
}

func TestBatch_RunShouldWorkWithUserCancellation(t *testing.T) {
	p := NewPool(PoolOptions{})
	p.Start()
	defer p.Stop()

	slowWork := func(ctx context.Context) (interface{}, error) {
		time.Sleep(time.Millisecond * 25)
		return "slooooow", nil
	}

	goodWork := func(ctx context.Context) (interface{}, error) {
		return "success", nil
	}

	ctx, cancel := context.WithCancel(context.Background())
	cancel()
	b := NewBatch(ctx, cancel, []WorkerFunc{slowWork, goodWork})
	_, err := b.Run(p)
	if !errors.Is(err, context.Canceled) {
		t.Fatalf("expected context cancelled error but got: %v", err)
	}
}

func TestBatch_RunShouldWork(t *testing.T) {
	p := NewPool(PoolOptions{})
	p.Start()
	defer p.Stop()

	slowWork := func(ctx context.Context) (interface{}, error) {
		time.Sleep(time.Millisecond * 25)
		return "slooooow", nil
	}

	goodWork := func(ctx context.Context) (interface{}, error) {
		return "success", nil
	}

	ctx, cancel := context.WithCancel(context.Background())
	b := NewBatch(ctx, cancel, []WorkerFunc{slowWork, goodWork})
	results, err := b.Run(p)
	if err != nil {
		t.Fatalf("expected error from failureWork, but got: nil")
	}

	want := []interface{}{"slooooow", "success"}
	if !reflect.DeepEqual(results, want) {
		t.Fatalf("got wrong results expected, want: %v, but got: %v", want, results)
	}
}

func TestBatch_RunShouldFailOnAnyJobsFailure(t *testing.T) {
	// limitation of workers is to avoid processing the second good job before the first has the chance to fail
	p := NewPool(PoolOptions{Workers: 1, JobQueue: 3})
	p.Start()
	defer p.Stop()

	failureWork := func(ctx context.Context) (interface{}, error) {
		return nil, fmt.Errorf("whoops")
	}

	slowWork := func(ctx context.Context) (interface{}, error) {
		time.Sleep(time.Millisecond * 250)
		return "slooooow", nil
	}

	goodWorkCalled := false
	goodWork := func(ctx context.Context) (interface{}, error) {
		goodWorkCalled = true
		return "success", nil
	}

	ctx, cancel := context.WithCancel(context.Background())
	b := NewBatch(ctx, cancel, []WorkerFunc{failureWork, slowWork, goodWork})
	_, err := b.Run(p)
	if goodWorkCalled {
		t.Errorf("good worker should not have been called upon failure of other job")
	}
	if err == nil {
		t.Errorf("expected error from failureWork, but got: nil")
	}
}

func TestPool_QueueCancelWorks(t *testing.T) {
	p := NewPool(PoolOptions{})
	p.Start()
	defer p.Stop()

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	blocker := make(chan struct{})
	defer close(blocker)

	job, err := p.Queue(ctx, func(ctx context.Context) (interface{}, error) {
		<-blocker
		return true, nil
	})
	if err != nil {
		t.Fatalf("unexpected error while queueing a new job: %v", err)
	}

	cancel() // simulate context cancellation
	res, err := job.Result()

	if res != nil && res.(bool) {
		t.Fatalf("expected job to block forever, but it resolved to: true")
	}

	if !errors.Is(err, context.Canceled) {
		t.Fatalf("expected job error to have been a cancellation err, but got: %v", err)
	}
}

func TestPool_QueueWorks(t *testing.T) {
	p := NewPool(PoolOptions{})
	p.Start()
	defer p.Stop()

	tests := []struct {
		name         string
		WorkerFunc   WorkerFunc
		shouldCancel bool
		want         interface{}
		wantErr      bool
	}{
		{
			name: "works on usual successful jobs",
			WorkerFunc: func(ctx context.Context) (interface{}, error) {
				time.Sleep(time.Millisecond * 23)
				return "success", nil
			},
			shouldCancel: false,
			want:         "success",
			wantErr:      false,
		},
		{
			name: "works on usual failed jobs",
			WorkerFunc: func(ctx context.Context) (interface{}, error) {
				time.Sleep(time.Millisecond * 21)
				return nil, fmt.Errorf("dang")
			},
			shouldCancel: false,
			want:         nil,
			wantErr:      true,
		},
		{
			name: "works on cancelled jobs",
			WorkerFunc: func(ctx context.Context) (interface{}, error) {
				blocker := make(chan struct{})
				<-blocker
				return "this should never be reached", nil
			},
			shouldCancel: true,
			want:         nil,
			wantErr:      true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			if tt.shouldCancel {
				cancel()
			}

			job, err := p.Queue(ctx, tt.WorkerFunc)
			if err != nil {
				t.Fatalf("unexpected error while queueing job: %v", err)
			}

			res, err := job.Result()
			if tt.wantErr && err == nil {
				t.Fatalf("expected error but got: nil")
			}
			if !tt.wantErr && err != nil {
				t.Fatalf("unexpected error for job: %v", err)
			}

			if !reflect.DeepEqual(res, tt.want) {
				t.Fatalf("result is different than expected, want: %v, but got: %v", tt.wantErr, res)
			}
		})
	}
}

func TestPool_QueueFailsOnStoppedPool(t *testing.T) {
	p := NewPool(PoolOptions{})
	_, err := p.Queue(context.Background(), func(ctx context.Context) (interface{}, error) {
		return true, nil
	})

	if err == nil {
		t.Fatalf("expected error, but got nil")
	}
}

func TestPool_StartStopWorks(t *testing.T) {
	p := NewPool(PoolOptions{})
	defer p.Stop() // cleanup

	ok := p.Start()
	if !ok {
		t.Fatalf("first start should work")
	}
	ok = p.Start()
	if ok {
		t.Fatalf("second start should fail")
	}

	ok = p.Stop()
	if !ok {
		t.Fatalf("first stop should work")
	}
	ok = p.Stop()
	if ok {
		t.Fatalf("first stop should fail")
	}
}
