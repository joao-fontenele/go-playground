package wp

type Logger interface {
	Info(msg string, keyValues ...interface{})
	Debug(msg string, keyValues ...interface{})
	Error(msg string, keyValues ...interface{})
}

type NoopLogger struct{}

func (n NoopLogger) Info(msg string, keyValues ...interface{})  {}
func (n NoopLogger) Debug(msg string, keyValues ...interface{}) {}
func (n NoopLogger) Error(msg string, keyValues ...interface{}) {}

var _ Logger = NoopLogger{}
