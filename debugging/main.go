package main

import (
	"fmt"
	"log"
	"net/http"

	"gitlab.com/joao-fontenele/go-playground/debugging/geo"
)

func main() {
	http.HandleFunc("/", hello)

	fmt.Println("server listening at :8080")
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func hello(w http.ResponseWriter, r *http.Request) {
	log.Printf("hello handler called")
	w.Header().Set("content-type", "application/json")

	rect := geo.NewRect(1, 2)

	_, err := fmt.Fprintf(w, "{\"message\": \"hello world!\", \"rect\": {\"area\": %d, \"aConst\": %d}}", rect.Area(), rect.StandingInfinity())
	if err != nil {
		log.Printf("error while writing response body: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}
