package geo

type Rectangle struct {
	Width  int
	Height int
}

func NewRect(width, height int) Rectangle {
	return Rectangle{
		Width:  width,
		Height: height,
	}
}

func (r Rectangle) Area() int {
	return r.area()
}

func (r Rectangle) StandingInfinity() int {
	return 8
}

func (r Rectangle) area() int {
	return r.Width * r.Height
}
