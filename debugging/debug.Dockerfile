FROM golang:1.19

RUN go install github.com/go-delve/delve/cmd/dlv@latest

RUN mv /go/bin/dlv /dlv
