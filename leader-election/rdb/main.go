package main

import (
	"context"
	"database/sql"
	"errors"
	"log"
	"os"
	"time"

	_ "github.com/lib/pq"
)

const (
	defaultLeaderTimeout = 30 * time.Second
	electionName         = "example_election"
)

var (
	node          = "unknown"
	leaderTimeout time.Duration
)

func main() {
	time.Sleep(time.Second * 5) // await db to be online
	var err error
	node, err = os.Hostname()
	if err != nil {
		log.Fatalf("failed to query node name as hostname: %v", err)
	}

	dbURI := os.Getenv("DB_URI")
	if dbURI == "" {
		log.Fatalf("pass environment variable DB_URI")
	}
	log.Printf("DB_URI=%v", dbURI)
	db, err := sql.Open("postgres", dbURI)
	if err != nil {
		log.Fatalf("failed to open database connection: %v", err)
	}
	defer db.Close()
	log.Printf("Successfully connected to the database! node=%v", node)

	leaderTimeout, err = time.ParseDuration(os.Getenv("LEADER_TIMEOUT"))
	if err != nil {
		leaderTimeout = defaultLeaderTimeout
		log.Printf("failed to parse LEADER_TIMEOUT, using defaultLeadetTimeout=%v", defaultLeaderTimeout.String())
	}
	if leaderTimeout == time.Duration(0) {
		log.Printf("invalid LEADER_TIMEOUT, using defaultLeadetTimeout=%v", defaultLeaderTimeout.String())
		leaderTimeout = defaultLeaderTimeout
	}

	ctx := context.Background()
	var leaderName string
	for {
		leaderName, err = acquireLeadership(ctx, db)
		if err != nil {
			log.Fatalf("failed to run initial leadership query: %v", err)
		}
		var sleepTime time.Duration
		if leaderName == node {
			log.Printf("node %v acquired leadership", node)
			sleepTime = leaderTimeout / 2
		} else {
			log.Printf("this node %v is not the leader, leaderNode=%v", node, leaderName)
			sleepTime = leaderTimeout
		}

		time.Sleep(sleepTime)
	}
}

func acquireLeadership(ctx context.Context, db *sql.DB) (leaderName string, err error) {
	tx, err := db.BeginTx(ctx, nil)
	if err != nil {
		return
	}
	defer func() {
		if err != nil {
			tx.Rollback()
			return
		}
		tx.Commit()
	}()

	// TODO: set the timeout from config
	_, err = tx.Exec("set local statement_timeout to 5000 ;") // 5000 ms
	if err != nil {
		return
	}

	// TODO: set the timeout from config
	_, err = tx.Exec("set local idle_in_transaction_session_timeout to 5000 ;") // 5000 ms
	if err != nil {
		return
	}

	var lastHeartbeat time.Time
	err = tx.QueryRowContext(ctx, "select node_name, last_heartbeat from leader where election_name=$1 for update;", electionName).Scan(&leaderName, &lastHeartbeat)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			err = nil
			return
		}
		return
	}

	if node != leaderName {
		timeSinceLastHeartbeat := time.Since(lastHeartbeat)
		if timeSinceLastHeartbeat < leaderTimeout {
			log.Printf("node=%v, last headerbeat is from %v ago, skipping attempting to acquire leadership", node, timeSinceLastHeartbeat)
			return
		}
	}

	_, err = tx.Exec("update leader set node_name=$1, last_heartbeat=$2 where election_name=$3;", node, time.Now().Format(time.RFC3339), electionName)
	if err != nil {
		return
	}

	leaderName = node
	return
}
