\connect leader;

create table leader (
    election_name varchar(64) PRIMARY KEY,
    node_name varchar(64),
    last_heartbeat timestamp with time zone
);

insert into leader (election_name, node_name, last_heartbeat) values ('example_election', 'initial', '2022-11-15T06:00:00.000-04');
