package main

import (
	"log"
	"net"
	"net/http"
)

func createListener() (net.Listener, func(), error) {
	// listener is needed to get the random open port at a later time. If you don´t need the port, just
	// `http.ListenAndServe(":0", handler)`
	l, err := net.Listen("tcp", ":0")
	if err != nil {
		return nil, nil, err
	}
	return l, func() {
		_ = l.Close()
	}, nil
}

func main() {
	l, closer, err := createListener()
	if err != nil {
		log.Fatalf("failed to create listener for random port: %v", err)
	}
	defer closer()

	http.Handle("/", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Printf("handler called")
		w.Header().Set("content-type", "application/json")
		w.WriteHeader(http.StatusOK)
	}))

	log.Println("listening at", l.Addr().(*net.TCPAddr).Port)
	log.Fatal(http.Serve(l, nil))
}
